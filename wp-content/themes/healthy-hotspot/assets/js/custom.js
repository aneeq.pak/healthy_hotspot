$(document).ready(function() {
    var active_tab_image = $('.home-tab-content').find('.tab.active .tab-image').attr('src');
    $('.home-tabbing').find('.changing-image').attr('src', active_tab_image);

    $('.community h2').each(function(index, value) {
        $(this).text(capitalize($(this).text()));
    });
    if ($(window).width() > 1250) {
        $('.primary-nav ul li a').first().hover(function(){
            //$('.sub-menu').hide();
            $('.primary-nav ul li .dropdown').css({'display' : 'block','opacity' : '0', 'z-index':'-1'});
            console.log('here');
            $(this).parent().find('.dropdown').css({'display' : 'block','opacity' : '1', 'z-index':'9999'});
        });
//        $('.primary-nav ul li .dropdown').mouseleave(function(){
//            //$('.sub-menu').hide();
//            console.log('here 1');
//            $('.primary-nav ul li .dropdown').css({'display' : 'block','opacity' : '0'});
//        });
     }
                    
    //Tabbing 
    $('.home-tabs a').click(function(event) {
        event.preventDefault();
        $('.home-tabs li').removeClass('active');
        $(this).closest('li').addClass('active');
        var currentTab = $(this).attr('href');
        $('.home-tab-content .tab').removeClass('active');
        $(currentTab).addClass('active');
        var active_tab_image = $('.home-tab-content').find('.tab.active .tab-image').attr('src');
        $('.home-tabbing').find('.changing-image').attr('src', active_tab_image);
    });
    //Tabbing
    $('.tab-wid ul a').click(function(event) {
        event.preventDefault();
        var currentTab = $(this).attr('href');
        $('.tab-content .tab').removeClass('active');
        $(currentTab).addClass('active');
    });
    $('.tabs-opener').click(function(event) {
        event.preventDefault();
        $(this).closest('.tab-wid').find('ul').slideToggle();
    });
    $(".slideToggle").click(function() {
        $(".listToggle").slideToggle('down');
        $(".fa").toggleClass('fa-sort-down');
    });

    $(".slideToggle1").click(function() {
        $(".listToggle1").slideToggle('down');
        $(".fa").toggleClass('fa-sort-down');
    });

    $(".slideToggle2").click(function() {
        $(".listToggle2").slideToggle('down');
        $(".fa").toggleClass('fa-sort-down');
    });
	
	$(".slideToggle3").click(function() {
        $(".listToggle3").slideToggle('down');
        $(".fa").toggleClass('fa-sort-down');
    });
	
	$(".slideToggle4").click(function() {
        $(".listToggle4").slideToggle('down');
        $(".fa").toggleClass('fa-sort-down');
    });
	
	$(".slideToggle5").click(function() {
        $(".listToggle5").slideToggle('down');
        $(".fa").toggleClass('fa-sort-down');
    });

    $('#menu-partner-header .menu-item-has-children').append('<span></span>');

    $("#menu-partner-header .menu-item-has-children > span").click(function() {
        $(this).closest('li').find(".sub-menu").slideToggle('down');
        $(this).toggleClass('downArrow');
    });

    lightbox.option({
        'resizeDuration': 200,
        'wrapAround': true
    })
	
	
	
	$('ul li:nth-child(5n)').addClass('fifth-child');
	$('ul li:nth-child(4n)').addClass('fourth-child');
	$('ul li:nth-child(3n)').addClass('third-item');
	$('ul li:first-child').addClass('first-item');
	$('ul li:last-child').addClass('last-item');
	$('.wrapper .box01:last-child,.wrapper .box02:last-child').addClass('last-item');
	$('ul li').addClass('odd-item');
	$('ul li:nth-child(2n)').removeClass('odd-item').addClass('even-item');
	$('.simple-list02 li:nth-child(4n)').addClass('fourth-child');


    //Filters dropdown
    $('.checks-drop .drop-opener').click(function() {
        $(this).closest('.checks-drop').find('.drop').slideToggle();
    });

    //Custom select
    $('.custom-select select').click(function() {
        $(this).closest('.custom-select').find('drop').slideDown();
    });
    //Menu opener
    $('.menu-opener').click(function(e) {
        e.preventDefault();
        $('#nav ul').slideToggle();
    });
    $('.primary-opener').click(function(e) {
        e.preventDefault();
        $('.primary-nav').slideToggle();
    });
    //HOvers
    $('.btn-popup').click(function(e) {
        e.preventDefault();
        var clicked_popup = $(this).attr('href');
        $(clicked_popup).addClass('active');
        var video_url = $(clicked_popup).find('iframe').prev().html(); //attr('src');
        //console.log(video_url);
        $(clicked_popup).find('iframe').attr('src', video_url);
        $(clicked_popup).find('iframe')[0].src += "&autoplay=1";
        $(document).on('keydown', function(e) {
            if (e.keyCode === 27) { // ESC
                $('.popup.active').find('iframe').attr('src', "");
                $('.popup.active').removeClass('active');
            }
        });
    });
    $('.popup-closer').click(function(e) {
        e.preventDefault();
        $('.popup.active').find('iframe').attr('src', "");
        $('.popup.active').removeClass('active');
    });
// Appened
    $('header .primary-nav .dropdown').append('<span class="menu-close"><i class="fa fa-window-close"></i></span>');
    // Menu Hide Show
    $('.menu-close').click(function () {
        $(this).parents('.dropdown').addClass('menu-opacity');
    });
    $('.primary-nav > ul > li > a').click(function () {
        $(this).parents('.primary-nav > ul > li').children('.dropdown').removeClass('menu-opacity');
    });
    
    $(window).scroll(function() {

        /* Check the location of each desired element */
        $('.animate').each(function(i) {

            var bottom_of_object = $(this).offset().top + 100;
            var bottom_of_window = $(window).scrollTop() + $(window).height();

            /* If the object is completely visible in the window, fade it it */
            if (bottom_of_window > bottom_of_object) {

                $(this).animate({ 'opacity': '1' }, 500);

            }

        });

    });

});

function capitalize(s) {
    return s.toLowerCase().replace(/\b./g, function(a) { return a.toUpperCase(); });
};

function updateTextInput(val) {
    document.getElementById('textInput').value = val;
}