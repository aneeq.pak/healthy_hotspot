<?php if (have_rows('healthy_hotspot_data', 'options')): ?>
    <ul class="thumbs add list-none">
        <?php while(have_rows('healthy_hotspot_data', 'options')): the_row(); ?>
            <li>
                <div class="d-table">
                    <div class="d-inline">
                        <img src="<?php echo get_sub_field('hotspot_image'); ?>" alt="icon">
                        <h2><?php echo get_sub_field('hotspot_heading'); ?></h2>
                        <div class="trans-div">
                            <?php echo get_sub_field('hotspot_text'); ?>
                            <?php if (get_sub_field('hotspot_link')): ?>
                                <a href="<?php echo get_sub_field('hotspot_link'); ?>" class="btn-primary"><?php echo get_sub_field('hotspot_link_text'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </li>
        <?php endwhile; ?>
    </ul>
<?php endif; ?>