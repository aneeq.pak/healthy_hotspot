<ul class="thumbs add list-none discoverWidget">
    <?php if (get_field('community_garden_') == 1) { ?>
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('community_garden_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('community_garden_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 335;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('community_garden_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('community_garden_link', 'options')): ?>
                            <a href="<?php echo get_field('community_garden_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('complete_streets_') == 1) { ?>    
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('complete_streets_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('complete_streets_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 541;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('complete_street_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('complete_street_link', 'options')): ?>
                            <a href="<?php echo get_field('complete_street_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('farmers_market_') == 1) { ?>    
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('farmers_market_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('farmers_market_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 539;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('farmers_market_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('farmers_market_link', 'options')): ?>
                            <a href="<?php echo get_field('farmers_market_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('healthy_corner_stores_') == 1) { ?>    
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('healthy_corner_store_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('healthy_corner_store_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 537;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('healthy_corner_store_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('healthy_corner_store_link', 'options')): ?>
                            <a href="<?php echo get_field('healthy_corner_store_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('medical_reserve_corps_') == 1) { ?>    
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('medical_reserve_corps_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('medical_reserve_corps_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 535;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('medical_reserve_corps_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('medical_reserve_corps_link', 'options')): ?>
                            <a href="<?php echo get_field('medical_reserve_corps_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('smoke_free_housing_') == 1) { ?>    
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('smoke_free_housing_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('smoke_free_housing_heading', 'options'); ?></h2>
                    <div class="trans-div">
                        <?php
                          $id = 533;
                          if (!empty(get_field('characters_limit', $id))):
                          echo get_field('characters_limit', $id);
                          endif;
                        ?>
                        <!--<p>
                            <?php
                            $text = get_field('smoke_free_housing_text', 'options');
                            echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                            ?>
                        </p>-->
                        <?php if (get_field('smoke_free_housing_link', 'options')): ?>
                            <a href="<?php echo get_field('smoke_free_housing_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('tobacco_21_') == 1) { ?>    
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('tobacco_21_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('tobacco_21_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 554;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('tobacco_21_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('tobacco_21_link', 'options')): ?>
                            <a href="<?php echo get_field('tobacco_21_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('walkbike_plan_') == 1) { ?>    
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('walkbike_plan_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('walkbike_plan_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 552;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('walkbike_plan_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('walkbike_plan_link', 'options')): ?>
                            <a href="<?php echo get_field('walkbike_plan_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('fit-friendly_worksites_') == 1) { ?>    
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('fit-friendly_worksites_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('fit-friendly_worksites_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 790;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('fit-friendly_worksites_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('fit-friendly_worksites_link', 'options')): ?>
                            <a href="<?php echo get_field('fit-friendly_worksites_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('smoke-free_workplaces_') == 1) { ?>   
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('smoke-free_workplaces_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('smoke-free_workplaces_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 799;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--$id = 799;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p>
                        <?php
                        $text = get_field('smoke-free_workplaces_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>-->
                        <?php if (get_field('smoke-free_workplaces_link', 'options')): ?>
                            <a href="<?php echo get_field('smoke-free_workplaces_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('healthy_schools_') == 1) { ?>   
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('healthy_schools_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('healthy_schools_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 806;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--
                        <p><?php
                        $text = get_field('healthy_schools_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>
                        -->
                        <?php if (get_field('healthy_schools_link', 'options')): ?>
                            <a href="<?php echo get_field('healthy_schools_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('smoke-free_collegeuniversity_') == 1) { ?>   
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('smoke-free_collegeuniversity_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('smoke-free_collegeuniversity_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 817;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('smoke-free_collegeuniversity_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?>
                        </p>-->

                        <?php if (get_field('smoke-free_collegeuniversity_link', 'options')): ?>
                            <a href="<?php echo get_field('smoke-free_collegeuniversity_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('forest_preserves_') == 1) { ?>   
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('forest_preserves_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('forest_preserves_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 831;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('forest_preserves_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>
                        -->
                        <?php if (get_field('forest_preserves_link', 'options')): ?>
                            <a href="<?php echo get_field('forest_preserves_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('smoke-free_parks_') == 1) { ?>   
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('smoke-free_parks_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('smoke-free_parks_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 825;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p><?php
                        $text = get_field('smoke-free_parks_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?></p>
                        -->
                        <?php if (get_field('smoke-free_parks_link', 'options')): ?>
                            <a href="<?php echo get_field('smoke-free_parks_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('smoke-free_hospitals_') == 1) { ?>   
        <li>
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('smoke-free_hospitals_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('smoke-free_hospitals_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 799;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>
                        <!--<p>
                        <?php
                        $text = get_field('smoke-free_hospitals_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?>
                        </p>-->

                        <?php if (get_field('smoke-free_hospitals_link', 'options')): ?>
                            <a href="<?php echo get_field('smoke-free_hospitals_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
    <?php if (get_field('healthy_house_of_worship_') == 1) { ?>   
        <li class="greenPan">
            <div class="d-table">
                <div class="d-inline">
                    <img src="<?php echo get_field('healthy_house_of_worship_image', 'options'); ?>" alt="icon">
                    <h2><?php echo get_field('healthy_house_of_worship_heading', 'options'); ?> </h2>
                    <div class="trans-div">
                        <?php
                        $id = 825;
                        if (!empty(get_field('characters_limit', $id))):
                            echo get_field('characters_limit', $id);
                        endif;
                        ?>

                        <!--<p>
                        <?php
                        $text = get_field('healthy_house_of_worship_text', 'options');
                        echo substr(str_replace(array('<p>', '</p>'), array('', ''), $text), 0, 120);
                        ?>
                        </p>-->

                        <?php if (get_field('healthy_house_of_worship_link', 'options')): ?>
                            <a href="<?php echo get_field('healthy_house_of_worship_link', 'options'); ?>" class="btn-primary">See More</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </li>
    <?php } ?>
</ul>
