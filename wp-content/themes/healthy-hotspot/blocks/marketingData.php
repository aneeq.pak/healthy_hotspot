<ul class="thumbs add list-none">
	
	<li>
		<div class="d-table">
			<div class="d-inline">
				<img src="<?php echo get_field('marketing_television_icon'); ?>" alt="icon">
				<h2>
					<?php echo get_field('marketing_television_heading'); ?>
				</h2>
				<div class="trans-div">
					<?php if (get_field('marketing_television_link')): ?>
					<a href="<?php echo get_field('marketing_television_link'); ?>" class="btn-primary">View Media</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</li>
	
	<li>
		<div class="d-table">
			<div class="d-inline">
				<img src="<?php echo get_field('marketing_radio_icon'); ?>" alt="icon">
				<h2>
					<?php echo get_field('marketing_radio_heading'); ?>
				</h2>
				<div class="trans-div">
					<?php if (get_field('marketing_radio_link')): ?>
					<a href="<?php echo get_field('marketing_radio_link'); ?>" class="btn-primary">View Media</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</li>
	
	<li>
		<div class="d-table">
			<div class="d-inline">
				<img src="<?php echo get_field('marketing_transit_icon'); ?>" alt="icon">
				<h2>
					<?php echo get_field('marketing_transit_heading'); ?>
				</h2>
				<div class="trans-div">
					<?php if (get_field('marketing_transit_link')): ?>
					<a href="<?php echo get_field('marketing_transit_link'); ?>" class="btn-primary">View Media</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</li>
	
	<li>
		<div class="d-table">
			<div class="d-inline">
				<img src="<?php echo get_field('marketing_online_icon'); ?>" alt="icon">
				<h2>
					<?php echo get_field('marketing_online_heading'); ?>
				</h2>
				<div class="trans-div">
					<?php if (get_field('marketing_online_link')): ?>
					<a href="<?php echo get_field('marketing_online_link'); ?>" class="btn-primary">View Media</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="d-table">
			<div class="d-inline">
				<img src="<?php echo get_field('marketing_print_icon'); ?>" alt="icon">
				<h2>
					<?php echo get_field('marketing_print_heading'); ?>
				</h2>
				<div class="trans-div">
					<?php if (get_field('marketing_print_link')): ?>
					<a href="<?php echo get_field('marketing_print_link'); ?>" class="btn-primary">View Media</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</li>
	<li>
		<div class="d-table">
			<div class="d-inline">
				<img src="<?php echo get_field('marketing_social_icon'); ?>" alt="icon">
				<h2>
					<?php echo get_field('marketing_social_heading'); ?>
				</h2>
				<div class="trans-div">
					<?php if (get_field('marketing_social_link')): ?>
					<a href="<?php echo get_field('marketing_social_link'); ?>" class="btn-primary">View Media</a>
					<?php endif; ?>
				</div>
			</div>
		</div>
	</li>
</ul>