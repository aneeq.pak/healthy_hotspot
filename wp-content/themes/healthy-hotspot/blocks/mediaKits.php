
<div class="mediaKits">
	<h2><?php echo get_field('social_media_kits_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$mediaKits = get_field('social_media_kits');
		?>
		<?php foreach ($mediaKits as $mediaKitsData): ?>
		<li>
			<figure>
				<img src="<?php echo $mediaKitsData['social_media_kits_image'];?>" alt="">
			</figure>
			<figcaption>
				<a href="<?php echo $mediaKitsData['social_media_kits_view_file'];?>" data-lightbox="roadtrip">
					Preview
				</a>
				<a class="downloadBtn" href="<?php echo $mediaKitsData['social_media_kits_download_file'];?>" target="_blank">
					Download
				</a>
			</figcaption>
		</li>
		<?php endforeach; ?>
	</ul>
</div>