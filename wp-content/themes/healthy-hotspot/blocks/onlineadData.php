<div class="onlineData">
	<h2><?php echo get_field('300_x_250_pixels_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$three250 = get_field('300_x_250_pixels_images');
		?>
		<?php foreach ($three250 as $three250Data): ?>
		<li>
			<a href="<?php echo $three250Data['300_x_250_pixels_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $three250Data['300_x_250_pixels_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>




<div class="onlineData">
	<h2><?php echo get_field('300_x_600_pixel_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$three600 = get_field('300_x_600_pixel_images');
		?>
		<?php foreach ($three600 as $three600Data): ?>
		<li>
			<a href="<?php echo $three600Data['300_x_600_pixel_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $three600Data['300_x_600_pixel_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="onlineData">
	<h2><?php echo get_field('320_x_50_pixels_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$three50 = get_field('320_x_50_pixels_images');
		?>
		<?php foreach ($three50 as $three50Data): ?>
		<li>
			<a href="<?php echo $three50Data['320_x_50_pixels_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $three50Data['320_x_50_pixels_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>




<div class="onlineData">
	<h2><?php echo get_field('320_x_480_pixel_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$three480 = get_field('320_x_480_pixel_images');
		?>
		<?php foreach ($three480 as $three480Data): ?>
		<li>
			<a href="<?php echo $three480Data['320_x_480_pixel_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $three480Data['320_x_480_pixel_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>




<div class="onlineData">
	<h2><?php echo get_field('970_x_250_pixel_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$nine250 = get_field('970_x_250_pixel_images');
		?>
		<?php foreach ($nine250 as $nine250Data): ?>
		<li>
			<a href="<?php echo $nine250Data['970_x_250_pixel_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $nine250Data['970_x_250_pixel_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>




<div class="onlineData">
	<h2><?php echo get_field('720_x_90_pixel_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$seven90 = get_field('720_x_90_pixel_images');
		?>
		<?php foreach ($seven90 as $seven90): ?>
		<li>
			<a href="<?php echo $seven90['720_x_90_pixel_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $seven90['720_x_90_pixel_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>



<div class="onlineData mobileData">
	<h2><?php echo get_field('mobile_video_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$mobileVid = get_field('mobile_video_links');
		?>
		<?php foreach ($mobileVid as $mobileVidData): ?>
		<li>
			<div class="video">
				<iframe width="589" height="332" src="<?php echo $mobileVidData['mobile_video_url'];?>" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>