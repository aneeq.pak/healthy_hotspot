<?php $partnersSlider = get_field('partners_data', 'options');
if ($partnersSlider): ?>
    <div class="flexslider carousel">
        <ul class="slides partners-list list-none">
                <?php foreach ($partnersSlider as $partners): ?>
                <li>
                        <?php if ($partners['partner_link']): ?>
                        <a href="<?php echo $partners['partner_link']; ?>" >
                        <?php endif; ?>
                        <img src="<?php echo $partners['partner_image']; ?>" alt="Logo">
                    <?php if ($partners['partner_link']): ?>
                        </a>
                <?php endif; ?>
                </li>       
    <?php endforeach; ?>
        </ul>
    </div>
<?php endif; ?>