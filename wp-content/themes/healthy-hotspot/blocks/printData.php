
<div class="printData">
	<h2><?php echo get_field('healthy_hotspot_heading'); ?></h2>
	<?php // check for rows (parent repeater)
		if( have_rows('healthy_hotspot_features') ): 
		while( have_rows('healthy_hotspot_features') ): the_row(); 
	?>
		<ul class="towCol">
			<li>
				<?php the_sub_field('healthy_hotspot_features_heading'); ?>
			</li>
			<li>
				<?php 
					if( have_rows('healthy_hotspot_features_links') ): 
					while( have_rows('healthy_hotspot_features_links') ): the_row();
				?>
				<span>
					<a href="<?php the_sub_field('hotspot_features_view_btn_file'); ?>" data-lightbox="roadtrip">
						<?php the_sub_field('hotspot_features_view_btn_text'); ?>
					</a>
				</span>
				
				<span>
					<a href="<?php the_sub_field('hotspot_features_download_btn_file'); ?>">
						<?php the_sub_field('hotspot_features_download_btn_text'); ?>
					</a>
				</span>
				<?php endwhile; endif; ?>
			</li>
		</ul>
	<?php endwhile; endif?>
</div>


<div class="printData">
	<h2><?php echo get_field('healthy_eating_heading'); ?></h2>
	<?php // check for rows (parent repeater)
		if( have_rows('healthy_eating_features') ): 
		while( have_rows('healthy_eating_features') ): the_row(); 
	?>
		<ul class="towCol">
			<li>
				<?php the_sub_field('healthy_eating_features_heading'); ?>
			</li>
			<li>
				<?php 
					if( have_rows('healthy_eating_features_links') ): 
					while( have_rows('healthy_eating_features_links') ): the_row();
				?>
				<span>
					<a href="<?php the_sub_field('eating_features_view_btn_file'); ?>" data-lightbox="roadtrip">
						<?php the_sub_field('eating_features_view_btn_text'); ?>
					</a>
				</span>
				
				<span>
					<a href="<?php the_sub_field('eating_features_download_btn_file'); ?>">
						<?php the_sub_field('eating_features_download_btn_text'); ?>
					</a>
				</span>
				<?php endwhile; endif; ?>
			</li>
		</ul>
	<?php endwhile; endif?>
</div>






<div class="printData">
	<h2><?php echo get_field('smoke_free_living_heading'); ?></h2>
	<?php // check for rows (parent repeater)
		if( have_rows('smoke_free_living_features') ): 
		while( have_rows('smoke_free_living_features') ): the_row(); 
	?>
		<ul class="towCol">
			<li>
				<?php the_sub_field('smoke_free_living_features_heading'); ?>
			</li>
			<li>
				<?php 
					if( have_rows('smoke_free_living_features_links') ): 
					while( have_rows('smoke_free_living_features_links') ): the_row();
				?>
				<span>
					<a href="<?php the_sub_field('smoke_living_view_btn_file'); ?>" data-lightbox="roadtrip">
						<?php the_sub_field('smoke_living_view_btn_text'); ?>
					</a>
				</span>
				<span>
					<a href="<?php the_sub_field('smoke_living_download_btn_file'); ?>">
						<?php the_sub_field('smoke_living_download_btn_text'); ?>
					</a>
				</span>
				
				<?php endwhile; endif; ?>
			</li>
		</ul>
	<?php endwhile; endif?>
</div>



<div class="printData">
	<h2><?php echo get_field('active_living_heading'); ?></h2>
	<?php // check for rows (parent repeater)
		if( have_rows('active_living_features') ): 
		while( have_rows('active_living_features') ): the_row(); 
	?>
		<ul class="towCol">
			<li>
				<?php the_sub_field('active_living_features_heading'); ?>
			</li>
			<li>
				<?php 
					if( have_rows('active_living_features_links') ): 
					while( have_rows('active_living_features_links') ): the_row();
				?>
				<span>
					
					<a href="<?php the_sub_field('living_features_view_btn_file'); ?>" data-lightbox="roadtrip">
						<?php the_sub_field('living_features_view_btn_text'); ?>
					</a>

				</span>
				<span>
					<a href="<?php the_sub_field('living_features_download_btn_file'); ?>">
						<?php the_sub_field('living_features_download_btn_text'); ?>
					</a>	
				</span>
				
				<?php endwhile; endif; ?>
			</li>
		</ul>
	<?php endwhile; endif?>
</div>
