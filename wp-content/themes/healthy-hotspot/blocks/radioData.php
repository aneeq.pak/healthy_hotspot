
<div class="radioData">
	<h2><?php echo get_field('radio_30-second_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$radio30 = get_field('radio_30-second_links');
		?>
		<?php foreach ($radio30 as $radio30Data): ?>
		<li>
			<div class="video">
				<iframe width="589" height="332" src="<?php echo $radio30Data['radio_30-second_url'];?>" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="radioData">
	<h2><?php echo get_field('radio_60-second_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$radio30 = get_field('radio_60-second_links');
		?>
		<?php foreach ($radio30 as $radio30Data): ?>
		<li>
			<div class="video">
				<iframe width="589" height="332" src="<?php echo $radio30Data['radio_60-second_url'];?>" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>