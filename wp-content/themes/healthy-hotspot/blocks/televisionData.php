
<div class="teleVideos">
	<h2><?php echo get_field('60-second_tv_ads_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$video60 = get_field('60-second_ads_videos');
		?>
		<?php foreach ($video60 as $video60Data): ?>
		<li>
			<div class="video">
				<iframe width="589" height="332" src="<?php echo $video60Data['60-second_ads_url'];?>" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="teleVideos">
	<h2><?php echo get_field('30-second_tv_ads_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$video30 = get_field('30-second_ads_videos');
		?>
		<?php foreach ($video30 as $video30Data): ?>
		<li>
			<div class="video">
				<iframe width="589" height="332" src="<?php echo $video30Data['30-second_ads_videos_url'];?>" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="teleVideos">
	<h2><?php echo get_field('5-second_tv_ads_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$video5 = get_field('5-second_ads_videos');
		?>
		<?php foreach ($video5 as $video5Data): ?>
		<li>
			<div class="video">
				<iframe width="589" height="332" src="<?php echo $video5Data['5-second_ads_video_url'];?>" frameborder="0" webkitallowfullscreen="" mozallowfullscreen="" allowfullscreen=""></iframe>
			</div>
		</li>
		<?php endforeach; ?>
	</ul>
</div>