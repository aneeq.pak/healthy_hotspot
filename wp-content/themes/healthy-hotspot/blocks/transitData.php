
<div class="transitData">
	<h2><?php echo get_field('bus_shelter_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$shelter = get_field('bus_shelter_images');
		?>
		<?php foreach ($shelter as $shelterData): ?>
		<li>
			<a href="<?php echo $shelterData['bus_shelter_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $shelterData['bus_shelter_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="transitData">
	<h2><?php echo get_field('bus_interior_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$interior = get_field('bus_interior_images');
		?>
		<?php foreach ($interior as $interiorData): ?>
		<li>
			<a href="<?php echo $interiorData['bus_interior_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $interiorData['bus_interior_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="transitData">
	<h2><?php echo get_field('bus_queen_tail_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$queen = get_field('bus_queen_tail_images');
		?>
		<?php foreach ($queen as $queenData): ?>
		<li>
			<a href="<?php echo $queenData['bus_queen_tail_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $queenData['bus_queen_tail_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>


<div class="transitData">
	<h2><?php echo get_field('outdoor_billboard_heading'); ?></h2>
	<ul class="towCol">
		<?php
			$billboard = get_field('outdoor_billboard_images');
		?>
		<?php foreach ($billboard as $billboardData): ?>
		<li>
			<a href="<?php echo $billboardData['outdoor_billboard_image'];?>" data-lightbox="roadtrip">
				<img src="<?php echo $billboardData['outdoor_billboard_image'];?>" alt="">
			</a>
		</li>
		<?php endforeach; ?>
	</ul>
</div>