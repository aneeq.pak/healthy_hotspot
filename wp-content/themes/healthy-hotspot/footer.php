<footer id="footer">
    <div class="container">
        <div class="fourcols">
            <div class="col">
                <?php if (is_active_sidebar('footer-section1')): ?>
                    <?php dynamic_sidebar('footer-section1'); ?>
                <?php endif; ?>
                <?php
                $twt = get_field('twitter_url', 'options');
                $fb = get_field('facebook_url', 'options');
                $gplus = get_field('google_plus_url', 'options');
                $linkedin = get_field('linkedin_url', 'options');
                if ($twt || $fb || $gplus || $linkedin):
                    ?>
                    <ul class="sharing list-none">
                        <?php if ($twt): ?>
                        <li><a href="<?php echo esc_url($twt); ?>" class="twitter">Twitter</a></li>
                        <?php endif; ?>
                        <?php if ($fb): ?>
                            <li><a href="<?php echo esc_url($fb); ?>" class="facebook">Facebook</a></li>
                        <?php endif; ?>
                        <?php if ($gplus): ?>
                            <li><a href="<?php echo esc_url($gplus); ?>" class="g-plus">Google plus</a></li>
                        <?php endif; ?>
                        <?php if ($linkedin): ?>
                            <li><a href="<?php echo esc_url($linkedin); ?>" class="linkdin">Linkdin</a></li>
                        <?php endif; ?>    
                    </ul>
                <?php endif; ?>
            </div>
            <?php if (is_active_sidebar('footer-section2')): ?>
                <div class="col">
                    <?php dynamic_sidebar('footer-section2'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-section3')): ?>
                <div class="col">
                    <?php dynamic_sidebar('footer-section3'); ?>
                </div>
            <?php endif; ?>
            <?php if (is_active_sidebar('footer-section4')): ?>
                <div class="col">
                    <?php dynamic_sidebar('footer-section4'); ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</footer>
 <div id="common-popup" class="popup">
    <div class="d-table">
        <div class="d-inline">
            <div class="holder container">
                <div style="display:none"><?php echo get_field('header_video','options')?></div>
                <iframe src="" frameborder="0"></iframe>
                <a href="#" class="popup-closer"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
            </div>
        </div>
    </div>
</div>
</div>
<?php wp_footer(); ?>
<script>
$(window).load(function() {
    $('.flexslider').flexslider({
        animation: "slide",
        animationLoop: true,
        itemWidth: 380,
        itemMargin: 5,
        pausePlay: true,
        start: function(slider) {
            $('body').removeClass('loading');
        }
    });
});
</script>
</body>
</html>