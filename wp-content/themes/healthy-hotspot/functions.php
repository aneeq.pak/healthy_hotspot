<?php
include( get_template_directory() . '/classes.php' );
include( get_template_directory() . '/widgets.php' );

add_action( 'themecheck_checks_loaded', 'theme_disable_cheks' );
function theme_disable_cheks() {
	$disabled_checks = array( 'TagCheck', 'Plugin_Territory', 'CustomCheck', 'EditorStyleCheck' );
	global $themechecks;
	foreach ( $themechecks as $key => $check ) {
		if ( is_object( $check ) && in_array( get_class( $check ), $disabled_checks ) ) {
			unset( $themechecks[$key] );
		}
	}
}
add_theme_support( 'automatic-feed-links' );
if ( !isset( $content_width ) ) {
	$content_width = 900;
}
remove_action( 'wp_head', 'wp_generator' );
add_action( 'after_setup_theme', 'theme_localization' );
function theme_localization () {
	load_theme_textdomain( 'healthy-hotspot', get_template_directory() . '/languages' );
}
/*
 * Let WordPress manage the document title.
 * By adding theme support, we declare that this theme does not use a
 * hard-coded <title> tag in the document head, and expect WordPress to
 * provide it for us.
 */
add_theme_support( 'title-tag' );

function theme_widget_init() {
        register_sidebar( array(
		'id'            => 'default-sidebar',
		'name'          => __( 'Default Sidebar', 'healthy-hotspot' ),
		'before_widget' => '<div class="widget %2$s" id="%1$s">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3>',
		'after_title'   => '</h3>'
	) );
	register_sidebar( array(
		'id'            => 'footer-section1',
		'name'          => __( 'Footer Section 1', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );
        register_sidebar( array(
		'id'            => 'footer-section2',
		'name'          => __( 'Footer Section 2', 'healthy-hotspot' ),
		'before_widget' => '<div class="wid">',
		'after_widget'  => '</div>',
		'before_title'  => '<strong class="title">',
		'after_title'   => '</strong>'
	) );
        register_sidebar( array(
		'id'            => 'footer-section3',
		'name'          => __( 'Footer Section 3', 'healthy-hotspot' ),
		'before_widget' => '<div class="wid">',
		'after_widget'  => '</div>',
		'before_title'  => '<strong class="title">',
		'after_title'   => '</strong>'
	) );
        register_sidebar( array(
		'id'            => 'footer-section4',
		'name'          => __( 'Footer Section 4', 'healthy-hotspot' ),
		'before_widget' => '<div class="wid">',
		'after_widget'  => '</div>',
		'before_title'  => '<strong class="title">',
		'after_title'   => '</strong>'
	) );
        register_sidebar( array(
		'id'            => 'discover-submenu',
		'name'          => __( 'Discover Submenu', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );
         register_sidebar( array(
		'id'            => 'learn-submenu',
		'name'          => __( 'Learn Submenu', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );
         register_sidebar( array(
		'id'            => 'find-submenu',
		'name'          => __( 'Find Hotspot Submenu', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );
          register_sidebar( array(
		'id'            => 'stories-submenu',
		'name'          => __( 'Stories Submenu', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );
        register_sidebar( array(
		'id'            => 'partners-submenu-left',
		'name'          => __( 'Partners Submenu Left', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );  
        register_sidebar( array(
		'id'            => 'partners-submenu-right',
		'name'          => __( 'Partners Submenu Right', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );
          register_sidebar( array(
		'id'            => 'healthy-hotspot-submenu',
		'name'          => __( 'Healthy HotSpot Menu', 'healthy-hotspot' ),
		'before_widget' => '',
		'after_widget'  => '',
		'before_title'  => '',
		'after_title'   => ''
	) );  
         
}
add_action( 'widgets_init', 'theme_widget_init' );

add_theme_support( 'post-thumbnails' );
set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
add_image_size( 'thumbnail_400x9999', 400, 9999, true );

register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'healthy-hotspot' ),
    'top' => __( 'Top Navigation', 'healthy-hotspot' ),
) );

//Add [email]...[/email] shortcode
function shortcode_email( $atts, $content ) {
	return antispambot( $content );
}
add_shortcode( 'email', 'shortcode_email' );

//Register tag [template-url]
function filter_template_url( $text ) {
	return str_replace( '[template-url]', get_template_directory_uri(), $text );
}
add_filter( 'the_content', 'filter_template_url' );
add_filter( 'widget_text', 'filter_template_url' );

//Register tag [site-url]
function filter_site_url( $text ) {
	return str_replace( '[site-url]', home_url(), $text );
}
add_filter( 'the_content', 'filter_site_url' );
add_filter( 'widget_text', 'filter_site_url' );

if( class_exists( 'acf' ) && !is_admin() ) {
	add_filter( 'acf/load_value', 'filter_template_url' );
	add_filter( 'acf/load_value', 'filter_site_url' );
}

//Replace standard wp menu classes
function change_menu_classes( $css_classes ) {
	return str_replace( array( 'current-menu-item', 'current-menu-parent', 'current-menu-ancestor' ), 'active', $css_classes );
}
add_filter( 'nav_menu_css_class', 'change_menu_classes' );

//Allow tags in category description
$filters = array( 'pre_term_description', 'pre_link_description', 'pre_link_notes', 'pre_user_description' );
foreach ( $filters as $filter ) {
	remove_filter( $filter, 'wp_filter_kses' );
}

//Make wp admin menu html valid
function wp_admin_bar_valid_search_menu( $wp_admin_bar ) {
	if ( is_admin() )
		return;

	$form  = '<form action="' . esc_url( home_url( '/' ) ) . '" method="get" id="adminbarsearch"><div>';
	$form .= '<input class="adminbar-input" name="s" id="adminbar-search" tabindex="10" type="text" value="" maxlength="150" />';
	$form .= '<input type="submit" class="adminbar-button" value="' . __( 'Search', 'healthy-hotspot' ) . '"/>';
	$form .= '</div></form>';

	$wp_admin_bar->add_menu( array(
		'parent' => 'top-secondary',
		'id'     => 'search',
		'title'  => $form,
		'meta'   => array(
			'class'    => 'admin-bar-search',
			'tabindex' => -1,
		)
	) );
}

function fix_admin_menu_search() {
	remove_action( 'admin_bar_menu', 'wp_admin_bar_search_menu', 4 );
	add_action( 'admin_bar_menu', 'wp_admin_bar_valid_search_menu', 4 );
}
add_action( 'add_admin_bar_menus', 'fix_admin_menu_search' );

//Disable comments on pages by default
function theme_page_comment_status( $post_ID, $post, $update ) {
	if ( !$update ) {
		remove_action( 'save_post_page', 'theme_page_comment_status', 10 );
		wp_update_post( array(
			'ID' => $post->ID,
			'comment_status' => 'closed',
		) );
		add_action( 'save_post_page', 'theme_page_comment_status', 10, 3 );
	}
}
add_action( 'save_post_page', 'theme_page_comment_status', 10, 3 );

//custom excerpt
function theme_the_excerpt() {
	global $post;
	
	if ( trim( $post->post_excerpt ) ) {
		the_excerpt();
	} elseif ( strpos( $post->post_content, '<!--more-->' ) !== false ) {
		the_content();
	} else {
		the_excerpt();
	}
}

//theme password form
function theme_get_the_password_form() {
	global $post;
	$post = get_post( $post );
	$label = 'pwbox-' . ( empty($post->ID) ? rand() : $post->ID );
	$output = '<form action="' . esc_url( site_url( 'wp-login.php?action=postpass', 'login_post' ) ) . '" class="post-password-form" method="post">
	<p>' . __( 'This content is password protected. To view it please enter your password below:', 'healthy-hotspot' ) . '</p>
	<p><label for="' . $label . '">' . __( 'Password:', 'healthy-hotspot' ) . '</label> <input name="post_password" id="' . $label . '" type="password" size="20" /> <input type="submit" name="Submit" value="' . esc_attr__( 'Submit' ) . '" /></p></form>
	';
	return $output;
}
add_filter( 'the_password_form', 'theme_get_the_password_form' );

function healthy_scripts_styles() {
	/*
	 * Adds JavaScript to pages with the comment form to support
	 * sites with threaded comments (when in use).
	 */
	wp_deregister_script( 'comment-reply' );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply', get_template_directory_uri() . '/js/comment-reply.js' );
	}

	// Loads JavaScript file with functionality specific.
       
	wp_enqueue_script( 'healthy-script','http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js', array( 'jquery' ) );
        wp_enqueue_script( 'main-script', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ) );
        wp_enqueue_script( 'slider-script', get_template_directory_uri() . '/assets/js/jquery.flexslider.js', array( 'jquery' ) );
		wp_enqueue_script( 'lightbox', get_template_directory_uri() . '/assets/js/lightbox.min.js', array( 'jquery' ) );
		wp_enqueue_script( 'custom-script', get_template_directory_uri() . '/assets/js/custom.js', array( 'jquery' ) );
        
	// Loads our main stylesheet.
	wp_enqueue_style( 'healthy-style', get_stylesheet_uri(), array() );
	
        // Implementation Slider.
	wp_enqueue_style( 'slider-flex', get_template_directory_uri() . '/assets/css/flexslider.css', array() );	
        
	// Implementation stylesheet.
	wp_enqueue_style( 'healthy-theme', get_template_directory_uri() . '/assets/css/all.css', array() );	
        
    // Implementation fontawesome.
	wp_enqueue_style( 'font-awesome', get_template_directory_uri() . '/assets/css/font-awesome.css', array() );	
	
	// Implementation lightbox.
	wp_enqueue_style( 'lightbox', get_template_directory_uri() . '/assets/css/lightbox.min.css', array() );	
        
        // Implementation fonts.
	wp_enqueue_style( 'google-fonts', 'https://fonts.googleapis.com/css?family=Lato:300,400,700,700i,900,900i|Montserrat:200,200i,300,300i,400,400i,500|Noto+Sans:400,400i,700,700i', array() );	
        
	// Loads the Internet Explorer specific stylesheet.
	wp_enqueue_style( 'healthy-ie', get_template_directory_uri() . '/css/ie.css' );
	wp_style_add_data( 'healthy-ie', 'conditional', 'lt IE 9' );
}
add_action( 'wp_enqueue_scripts', 'healthy_scripts_styles' );


//theme options tab in appearance
if (function_exists("register_options_page")) {
    register_options_page('Theme Options');
    register_options_page('Header');
    register_options_page('Footer');
}

//acf theme functions placeholders
if( !class_exists( 'acf' ) && !is_admin() ) {
	function get_field_reference( $field_name, $post_id ) { return ''; }
        
	function get_field_objects( $post_id = false, $options = array() ) { return false; }
	function get_fields( $post_id = false ) { return false; }
	function get_field( $field_key, $post_id = false, $format_value = true )  { return false; }
	function get_field_object( $field_key, $post_id = false, $options = array() ) { return false; }
	function the_field( $field_name, $post_id = false ) {}
	function have_rows( $field_name, $post_id = false ) { return false; }
	function the_row() {}
	function reset_rows( $hard_reset = false ) {}
	function has_sub_field( $field_name, $post_id = false ) { return false; }
	function get_sub_field( $field_name ) { return false; }
	function the_sub_field( $field_name ) {}
	function get_sub_field_object( $child_name ) { return false;}
	function acf_get_child_field_from_parent_field( $child_name, $parent ) { return false; }
	function register_field_group( $array ) {}
	function get_row_layout() { return false; }
	function acf_form_head() {}
	function acf_form( $options = array() ) {}
	function update_field( $field_key, $value, $post_id = false ) { return false; }
	function delete_field( $field_name, $post_id ) {}
	function create_field( $field ) {}
	function reset_the_repeater_field() {}
	function the_repeater_field( $field_name, $post_id = false ) { return false; }
	function the_flexible_field( $field_name, $post_id = false ) { return false; }
	function acf_filter_post_id( $post_id ) { return $post_id; }
}
add_action( 'init', 'theme_custom_init' );
function theme_custom_init() {
  $labels = array(
    'name' => _x('Success Stories', 'post type general name', 'your_text_domain'),
    'singular_name' => _x('Success Story', 'post type singular name', 'your_text_domain'),
    'add_new' => _x('Add New', 'Success Story', 'your_text_domain'),
    'add_new_item' => __('Add New Success Story', 'your_text_domain'),
    'edit_item' => __('Edit Success Story', 'your_text_domain'),
    'new_item' => __('New Success Story', 'your_text_domain'),
    'all_items' => __('All Success Stories', 'your_text_domain'),
    'view_item' => __('View Success Stories', 'your_text_domain'),
    'search_items' => __('Search Success Stories', 'your_text_domain'),
    'not_found' =>  __('No Success Stories found', 'your_text_domain'),
    'not_found_in_trash' => __('No Success Stories found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Success Stories', 'your_text_domain')

  );
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'thumbnail','custom-fields','excerpt'  )
  ); 
  $labels1 = array(
    'name' => _x('Healthy Hotspots', 'post type general name', 'your_text_domain'),
    'singular_name' => _x('Healthy Hotspot', 'post type singular name', 'your_text_domain'),
    'add_new' => _x('Add New', 'Healthy Hotspot', 'your_text_domain'),
    'add_new_item' => __('Add New Healthy Hotspot', 'your_text_domain'),
    'edit_item' => __('Edit Healthy Hotspot', 'your_text_domain'),
    'new_item' => __('New Healthy Hotspot', 'your_text_domain'),
    'all_items' => __('All Healthy Hotspots', 'your_text_domain'),
    'view_item' => __('View Healthy Hotspots', 'your_text_domain'),
    'search_items' => __('Search Healthy Hotspots', 'your_text_domain'),
    'not_found' =>  __('No Healthy Hotspots found', 'your_text_domain'),
    'not_found_in_trash' => __('No Healthy Hotspots found in Trash', 'your_text_domain'), 
    'parent_item_colon' => '',
    'menu_name' => __('Healthy Hotspots', 'your_text_domain')

  );
  $args1 = array(
    'labels' => $labels1,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true,
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'supports' => array( 'title', 'editor', 'thumbnail','custom-fields','excerpt'  )
  ); 
  register_post_type('stories', $args);
  register_post_type('healthy-hotspots', $args1);
}
add_action( 'init', 'hotspot_categories' );
function hotspot_categories() {
 $labels = array(
    'name' => _x( 'Hotspot Locations', 'taxonomy general name' ),
    'singular_name' => _x( 'Hotspot Location', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Hotspot Locations' ),
    'all_items' => __( 'All Hotspot Locations' ),
    'parent_item' => __( 'Parent Hotspot Location' ),
    'parent_item_colon' => __( 'Parent Hotspot Location:' ),
    'edit_item' => __( 'Edit Hotspot Location' ),
    'update_item' => __( 'Update Hotspot Location' ),
    'add_new_item' => __( 'Add New Hotspot Location' ),
    'new_item_name' => __( 'New Hotspot Location Name' ),
  ); 
   register_taxonomy('hotspot_taxonomies','healthy-hotspots',array(
    'hierarchical' => true,
    'show_admin_column' => true,
    'labels' => $labels
	
  ));
    $labels2 = array(
    'name' => _x( 'Tags', 'taxonomy general name' ),
    'singular_name' => _x( 'Tag', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Tags' ),
    'popular_items' => __( 'Popular Tags' ),
    'all_items' => __( 'All Tags' ),
    'parent_item' => null,
    'parent_item_colon' => null,
    'edit_item' => __( 'Edit Tag' ), 
    'update_item' => __( 'Update Tag' ),
    'add_new_item' => __( 'Add New Tag' ),
    'new_item_name' => __( 'New Tag Name' ),
    'separate_items_with_commas' => __( 'Separate tags with commas' ),
    'add_or_remove_items' => __( 'Add or remove tags' ),
    'choose_from_most_used' => __( 'Choose from the most used tags' ),
    'menu_name' => __( 'Tags' ),
  ); 

  register_taxonomy('tag','healthy-hotspots',array(
    'hierarchical' => false,
    'labels' => $labels2,
    'show_ui' => true,
    'update_count_callback' => '_update_post_term_count',
    'query_var' => true,
    'rewrite' => array( 'slug' => 'tag' ),
  ));
 
}

function title_filter($where, &$wp_query){
    global $wpdb;
    if($search_term = $wp_query->get( 'title_filter' )){
        $search_term = $wpdb->esc_like($search_term); //instead of esc_sql()
        $search_term = ' \'%' . $search_term . '%\'';
        $title_filter_relation = (strtoupper($wp_query->get( 'title_filter_relation'))=='OR' ? 'OR' : 'AND');
        $where .= ' '.$title_filter_relation.' ' . $wpdb->posts . '.post_title LIKE '.$search_term;
    }
    return $where;
}

function my_acf_init() {
	acf_update_setting('google_api_key', 'AIzaSyCV3FX8T3vo7mMEXg--_0TPFaXU-1iUiOs');
}
add_action('acf/init', 'my_acf_init');