<!DOCTYPE html>
<html <?php language_attributes(); ?>>
    <head>
        <meta charset="<?php bloginfo('charset'); ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title><?php wp_title(' | ', true, 'right'); ?><?php bloginfo('name'); ?></title>
        <script type="text/javascript">
            var pathInfo = {
                healthy: '<?php echo get_template_directory_uri(); ?>/',
                css: 'css/',
                js: 'js/',
            }
        </script>
        <?php wp_head(); ?>
    </head>
    <body <?php body_class(); ?>>
        <div id="wrapper">
            <header id="header">
                <div class="top">
                   <div class="container">
						<a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="Healthy Hotspot"></a>
						<?php echo do_shortcode('[google-translator]'); ?>
						<!--<a href="#" class="lang">Language <i class="fa fa-sort-desc" aria-hidden="true"></i></a>-->
						<nav id="nav">
							<?php
							if (has_nav_menu('top'))
								wp_nav_menu(array(
									'container' => false,
									'theme_location' => 'top',
									'menu_class' => 'list-none',
									'items_wrap' => '<ul class="%2$s">%3$s</ul>'
										)
								);
							?>

						</nav>
						<a href="#" class="menu-opener">HEALTHY HOTSPOT</a>
              		</div>
                </div>
                <div class="primary-header">
                    <a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-new.png" alt="Healthy Hotspot"></a>
                    <a href="#" class="primary-opener"><i class="fa fa-bars" aria-hidden="true"></i></a>
                    <nav class="primary-nav">
                        <?php /* if( has_nav_menu( 'primary' ) )
                          wp_nav_menu( array(
                          'container' => false,
                          'theme_location' => 'primary',
                          'menu_class'     => 'list-none',
                          'items_wrap'     => '<ul class="%2$s">%3$s</ul>',
                          'walker'         => new Custom_Walker_Nav_Menu
                          )
                          ); */ ?>


                        <ul class="list-none">
                            <li><a href="<?php echo home_url(); ?>">Home</a></li>
                            <li>
                                <a href="#">Discover</a>
                                <div class="dropdown">
                                    <?php dynamic_sidebar('discover-submenu'); ?>
                                </div>
                            </li>
                            <li>
                                <a href="#">Learn</a>
                                <div class="dropdown">
                                    <?php dynamic_sidebar('learn-submenu'); ?>
                                </div>
                            </li>
                            <li>
                                <a href="#">Find Hotspot near you</a>
                                <div class="dropdown">
                                    <?php dynamic_sidebar('find-submenu'); ?>
                                </div>
                            </li>
                            <li>
                                <a href="#">Stories</a>
                                <div class="dropdown">
                                    <?php dynamic_sidebar('stories-submenu'); ?>
                                </div>
                            </li>
                            <li class="partnersNav">
                                <a href="#">Partners</a>
                                <div class="dropdown">
                                    <div class="align-left partnersNav">
                                        <?php dynamic_sidebar('partners-submenu-left'); ?>
                                    </div>
                                    <?php dynamic_sidebar('partners-submenu-right'); ?>
                                </div>
                            </li>
                            <?php if(get_field('healthy_hotspot_menu','options')==1):?>
                            <li class="becomeMember">
                                <a href="#"><?php echo get_field('menu_item_title','options');?> </a>
                                <div class="dropdown">
                                    <?php dynamic_sidebar('healthy-hotspot-submenu'); ?>
                                </div>
                            </li>
                            <?php endif; ?>
                        </ul>
                    </nav>
                </div>

            </header>



