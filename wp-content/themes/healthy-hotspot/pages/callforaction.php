<?php
/*
  Template Name: Call For Action Template
 */
get_header();
?>
<main id="main">
    <?php while (have_posts()): the_post(); ?>
        <article class="visual">
            <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>    
            <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="container">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <div class="community container">
            <div class="cols">
                <?php the_content(); ?>
            </div>
         
        </div>
        <div class="discover">
            <header class="sec-header">
                <strong class="small-title"><?php echo get_field('applicants_heading'); ?></strong>
                <?php echo get_field('applicants_text'); ?>
            </header>
            <article class="visual">
                <?php $img2 = get_field('applicants_image'); ?>    
                <img src="<?php echo $img2; ?>" alt="Promo image" class="img-responsive">
                <div class="caption eligible-text">
                    <div class="d-table">
                        <div class="d-inline">
                            <div class="container">
                                <?php echo get_field('applicants_image_text'); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </article>
        </div>
        <div class="community container">
            <h2><?php echo get_field('eligible_projects_heading'); ?></h2>
            <?php echo get_field('eligible_projects_text'); ?>
        </div>
        <div class="discover">
            <?php if (have_rows('eligible_projects')): ?>
                <ul class="discover-tiles full-opacity list-none">
                    <?php while (have_rows('eligible_projects')): the_row(); ?>
                        <li class="animate">
                            <?php $simg = get_sub_field('project_image'); ?>    
                            <div class="align-left">
                                <img src="<?php echo $simg; ?>" alt="Image" class="img-responsive">
                            </div>
                            <div class="align-right">
                                <div class="d-table">
                                    <div class="d-inline">
                                        <h3><?php echo get_sub_field('project_heading'); ?> </h3>
                                        <?php echo get_sub_field('project_content'); ?> 
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
                <?php
            endif;
            ?>
        </div>
        <div class="community container">
            <?php echo get_field('essential_elements_data'); ?>
        </div>
      <div class="map-area">
            <img src="<?php echo get_field('success_map_image', 'options'); ?>" alt="Map" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="custom-container">
                            <div class="txt">
                                <strong class="small-title"><?php echo get_field('success_map_heading', 'options'); ?></strong>
                                <h2><?php echo get_field('success_map_content', 'options'); ?></h2>
                            </div>
                            <a href="<?php echo get_field('success_map_link', 'options'); ?>" class="btn-primary"><?php echo get_field('success_map_link_text', 'options'); ?></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>