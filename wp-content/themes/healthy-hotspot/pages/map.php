<?php
/*
  Template Name: Map Template
 */
get_header();
?>
<main id="main">
     <?php while (have_posts()): the_post(); ?>
    <article class="visual">
        <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>
        <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
        <div class="caption">
            <div class="d-table">
                <div class="d-inline">
                    <div class="container">
                        <h2><?php the_title(); ?></h2>
                    </div>
                </div>
            </div>
        </div>
    </article>
     <?php endwhile; ?>
    <div id="map"></div>
    <div class="finding-area">
        <aside id="sidebar">
            <h2>Find Hotspots</h2>
            <form action="" class="find-form">
                <?php
                $taxonomies = get_terms(array('taxonomy' => 'hotspot_taxonomies', 'hide_empty' => 0));
                if ($taxonomies):
                    ?>
                    <div class="custom-select">
                        <select name="place">
                            <?php foreach ($taxonomies as $terms): ?>
                                <option <?php if ($_GET['place'] == $terms->slug) echo 'selected'; ?> value="<?php echo $terms->slug; ?>"><?php echo $terms->name ?></option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                <?php endif; ?>
                <input type="text" name="map-keyword" placeholder="Keywords" value="<?php echo $_GET['map-keyword'] ?>">
                <input type="text" name="map-zipAddress" placeholder="Address or zip" value="<?php echo $_GET['map-zipAddress'] ?>">

                <input type="submit" value="search" name="map-search" class="btn-primary">
            </form>
            <div class="range-slider">
                <input type="range" name="rangeInput" min="0" max="25" onchange="updateTextInput(this.value);" >
                <input type="text" class="hidden" id="textInput">
            </div>
            <span class="notice">search in radius <em id="radious_search">25</em> miles</span>
            <form action="#" class="checks-form">
                <fieldset>
                    <div class="checks-drop">
                        <input type="checkbox" id="w-live" class="strategy_check" >
                        <label for="w-live" class="drop-opener">where we live</label>
                        <div class="drop">
                            <input type="checkbox" id="c-gardens" value="community_gardens" class="strategy_check we-live" >
                            <label for="c-gardens">community gardens</label>
                            <input type="checkbox" id="complete-streets" value="complete_streets" class="strategy_check we-live" >
                            <label for="complete-streets">complete streets</label>
                            <input type="checkbox" selected id="farmer-markets" value="farmers_markets" class="strategy_check we-live" >
                            <label for="farmer-markets">farmer's markets</label>
                            <input type="checkbox" selected id="healthy-corner" value="healthy_corner_stores" class="strategy_check we-live" >
                            <label for="healthy-corner">healthy corner stores</label>
                            <input type="checkbox" id="medical-reserve" value="medical_reserve_corps" class="strategy_check we-live" >
                            <label for="medical-reserve">medical reserve corps</label>
                            <input type="checkbox" id="smoke-free" value="smoke_free_housing" class="strategy_check we-live" >
                            <label for="smoke-free">Smoke-Free housing</label>
                            <input type="checkbox" selected id="walk-bike" value="walk_bike_plan" class="strategy_check we-live" >
                            <label for="walk-bike">walk/bike plan</label>
                            <input type="checkbox" id="tobacco" value="tobacco_21" class="strategy_check we-live" >
                            <label for="tobacco">tobacco 21</label>
                        </div>
                    </div>
                    <div class="checks-drop">
                        <input  type="checkbox" id="w-w" class="strategy_check">
                        <label for="w-w" class="drop-opener">where we work</label>
                        <div class="drop">
                            <input  type="checkbox" id="fit-frendly-worksites" value="fit_friendly_worksites" class="strategy_check www">
                            <label for="fit-frendly-worksites">Fit-Friendly Worksites</label>
                            <input  type="checkbox" id="smoke-Free_workplaces" value="smoke_free_workplaces" class="strategy_check www">
                            <label for="smoke-Free_workplaces">Smoke-Free Workplaces</label>
                        </div>
                    </div>
                    <div class="checks-drop">
                        <input  type="checkbox" id="w-learn" class="strategy_check">
                        <label for="w-learn" class="drop-opener">where we learn</label>
                        <div class="drop">
                            <input  type="checkbox" id="healthy_schools" value="healthy_schools" class="strategy_check w-learn">
                            <label for="healthy_schools">Healthy Schools</label>
                            <input  type="checkbox" id="smoke_free_college_university" value="smoke_free_college_university" class="strategy_check w-learn">
                            <label for="smoke_free_college_university">Smoke-Free College/University</label>
                        </div> 
                    </div>
                    <div class="checks-drop">
                        <input  type="checkbox" id="w-play" class="strategy_check">
                        <label for="w-play" class="drop-opener">where we play</label>
                        <div class="drop">
                            <input  type="checkbox" id="forest_preserves" value="forest_preserves_of_cook_county" class="strategy_check w-play">
                            <label for="forest_preserves_of_cook_county">Forest Preserves of Cook County</label>
                            <input  type="checkbox" id="smoke_free_parks" value="smoke_free_parks" class="strategy_check w-play">
                            <label for="smoke_free_parks">Smoke-Free Parks</label>
                        </div> 
                    </div>
                    <div class="checks-drop">
                        <input  type="checkbox" id="w-receive" class="strategy_check">
                        <label for="w-receive" class="drop-opener">where we receive healthcare</label>
                        <div class="drop">
                            <input  type="checkbox" id="smoke_free_hospitals" value="smoke_free_hospitals" class="strategy_check w-receive">
                            <label for="smoke_free_hospitals">Smoke-Free Hospitals</label>
                        </div> 
                    </div>
                    <div class="checks-drop">
                        <input  type="checkbox" id="w-worship" class="strategy_check">
                        <label for="w-worship" class="drop-opener">where we worship</label>
                        <div class="drop">
                            <input  type="checkbox" id="healthy_house_of_worship" value="healthy_house_of_worship" class="strategy_check w-worship">
                            <label for="healthy_house_of_worship">Healthy House of Worship</label>
                        </div> 
                    </div>
                </fieldset>
            </form>
        </aside>
        <?php function getDistance($point1_lat, $point1_long, $point2_lat, $point2_long, $unit = 'km', $decimals = 2) {
            // Calculate the distance in degrees
            $degrees = rad2deg(acos((sin(deg2rad($point1_lat)) * sin(deg2rad($point2_lat))) + (cos(deg2rad($point1_lat)) * cos(deg2rad($point2_lat)) * cos(deg2rad($point1_long - $point2_long)))));

            // Convert the distance in degrees to the chosen unit (kilometres, miles or nautical miles)
            switch ($unit) {
                case 'km':
                    $distance = $degrees * 111.13384; // 1 degree = 111.13384 km, based on the average diameter of the Earth (12,735 km)
                    break;
                case 'mi':
                    $distance = $degrees * 69.05482; // 1 degree = 69.05482 miles, based on the average diameter of the Earth (7,913.1 miles)
                    break;
                case 'nmi':
                    $distance = $degrees * 59.97662; // 1 degree = 59.97662 nautic miles, based on the average diameter of the Earth (6,876.3 nautical miles)
            }
            return round($distance, $decimals);
        }

        if (!empty($_GET)):
            ?>
            <?php
            add_filter('posts_where','title_filter', 10, 2);
            $place = $_GET['place'];
            $keyword = $_GET['map-keyword'];
            $zipAddress = $_GET['map-zipAddress'];
            if ($place != '' || $keyword != '' || $zipAddress != '') {
                $args = array(
                    'posts_per_page' => -1,
                    'post_type' => 'healthy-hotspots',
                    'title_filter' => $keyword,
                    'title_filter_relation' => 'OR',
                    'post_status' => 'publish',
                    'meta_query' => array(
                        'relation' => 'OR',
                        array(
                            'key' => 'hotspot_city',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_address',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_strategy',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_zip',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_state',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_phone',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'healthy_hotspot_activity',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'healthy_website',
                            'value' => $keyword,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_zip',
                            'value' => $zipAddress,
                            'compare' => 'LIKE'
                        ),
                        array(
                            'key' => 'hotspot_address',
                            'value' => $zipAddress,
                            'compare' => 'LIKE'
                        )
                    ),
                    'tax_query' => array(
                        array(
                            'taxonomy' => 'hotspot_taxonomies',
                            'field' => 'slug',
                            'terms' => $place
                        )
                    )
                );
            }
            remove_filter('posts_where', 'title_filter', 10, 2);
//            } elseif ($place != '' && $keyword != '' && $zipAddress == '') {
//                
//            } elseif ($place != '' && $keyword == '' && $zipAddress == '') {
//                
//            } elseif ($place != '' && $keyword == '' && $zipAddress != '') {
//                
//            } elseif ($place == '' && $keyword != '' && $zipAddress != '') {
//                
//            } elseif ($place == '' && $keyword != '' && $zipAddress == '') {
//                
//            } elseif ($place == '' && $keyword == '' && $zipAddress != '') {
//                
//            }
            ?>
            <?php
        else:
            $args = array('posts_per_page' => -1, 'post_type' => 'healthy-hotspots');
            ?>                   
        <?php endif; ?>
        <?php $query = new WP_Query($args); ?>
        <?php
        if ($query->have_posts()):

            require_once( trailingslashit(get_template_directory()) . 'geoplugin.class.php' );
            $geoplugin = new geoPlugin();
            $geoplugin->locate();
            $lat = $geoplugin->latitude;
            $lng = $geoplugin->latitude;
            ?>
            <div id="content">
                <?php $hotspot = array(); ?>
                <?php while ($query->have_posts()): $query->the_post(); ?>
                    <?php
                    $terms = get_the_terms(get_the_ID(), 'hotspot_taxonomies');
                    $hotspot_place = $terms[0]->name;
                    $hotspot_strategy = get_field('hotspot_strategy');
                    $hotspot_city = get_field('hotspot_city');
                    $hotspot_zip = get_field('hotspot_zip');
                    $hotspot_address = get_field('hotspot_address');
                    $hotspot_state = get_field('hotspot_state');
                    $hotspot_phone = get_field('hotspot_phone');
                    $healthy_hotspot_activity = get_field('healthy_hotspot_activity');
                    $healthy_website = get_field('healthy_website');
                    $healthy_marker = get_field('map_marker_image');
                    $hotspot_latetude = get_field('hotspot_latetude');
                    $hotspot_longitude = get_field('hotspot_longitude');
                    
                    $distance = getDistance($lat, $lng, $hotspot_latetude, $hotspot_longitude, "mi", 2);
                    $hotspot[] = array('hotspot_strategy' => strtolower(str_replace(array(' ','-','/'),array('_','_','_'),$hotspot_strategy)), 'hotspot_place' => $hotspot_place, 'hotspot_address' => $hotspot_address, 'hotspot_zip' => $hotspot_zip, 'hotspot_lat' => $hotspot_latetude, 'hotspot_long' => $hotspot_longitude, 'distance' => $distance, 'marker_image' => $healthy_marker);
                    ?>
                    <article class="post" data-strategy="<?php echo strtolower(str_replace(array(' ','-','/'),array('_','_','_'),$hotspot_strategy)); ?>" data-latlong="<?= $lat . ' ' . $lng . ' -- ' . $hotspot_latetude . ' ' . $hotspot_longitude; ?>">
                        <h2><?php the_title(); ?></h2>
                        <strong class="title strategy-title"><?php echo str_replace('_', ' ', $hotspot_strategy); ?></strong>
                        <p><?php echo $healthy_hotspot_activity; ?></p>
                        <div class="features">
                            <dl>
                                <dt>City:</dt>
                                <dd><?php echo $hotspot_city; ?></dd>
                                <dt>Zip:</dt>
                                <dd><?php echo $hotspot_zip; ?></dd>
                                <dt>Place:</dt>
                                <dd><?php echo $hotspot_place; ?></dd>
                            </dl>
                            <dl>

                                <dt>Address:</dt>
                                <dd><?php echo $hotspot_address; ?></dd>
                                <dt>State:</dt>
                                <dd><?php echo $hotspot_state; ?></dd>
                                <dt>Phone:</dt>
                                <dd><?php echo $hotspot_phone; ?></dd>
                            </dl>
                        </div>
                        <ul class="tags list-none">
                            <li><a href="#"><i class="fa fa-tag" aria-hidden="true"></i> Tag Label</a></li>
                            <li class="red"><a href="#"><i class="fa fa-tag" aria-hidden="true"></i> Tag Label</a></li>
                            <li class="blue"><a href="#"><i class="fa fa-tag" aria-hidden="true"></i> Tag Label</a></li>
                            <li class="dark-pink"><a href="#"><i class="fa fa-tag" aria-hidden="true"></i> Tag Label</a></li>
                        </ul>
                    </article>
    <?php endwhile; //print_r($hotspot);   ?>

            </div>
            <?php
        endif;
        wp_reset_query();
        ?>
    </div>
    <div class="home-tabbing animate">
        <div class="col">
<?php $pageID = get_page_by_path('home'); ?>
            <strong class="small-title"><?php echo get_field('learning_section_heading', $pageID); ?></strong>
            <h1><?php echo get_field('learning_block_heading', $pageID); ?></h1>
                <?php if (have_rows('learning_block_tabbing', $pageID)) { ?>
                <ul class="home-tabs list-none">
                    <?php $count = 1;
                    while (have_rows('learning_block_tabbing', $pageID)): the_row();
                        ?>
                        <li <?php
                            if ($count == 1) {
                                echo 'class="active"';
                            }
                            ?>><a href="#<?php echo get_sub_field('learn_tab_id', $pageID); ?>"><?php echo get_sub_field('learn_tab_heading', $pageID); ?></a></li>
                    <?php
                    $count++;
                endwhile;
                ?>
                </ul>
                <?php }; ?>
                <?php if (have_rows('learning_block_tabbing', $pageID)) { ?>
                <div class="home-tab-content">
                        <?php
                        $countTab = 1;
                        while (have_rows('learning_block_tabbing', $pageID)): the_row();
                            ?>
                        <div id="<?php echo get_sub_field('learn_tab_id', $pageID); ?>" class="tab <?php if ($countTab == 1) echo 'active'; ?>">
        <?php echo get_sub_field('learn_tab_text', $pageID); ?>
                            <div class="three-cols">
                                <?php if (get_sub_field('hotspot_figure', $pageID)): ?>
                                    <div class="column">
                                        <span class="counter"><i><?php echo get_sub_field('hotspot_figure', $pageID); ?></i></span>
                                        <em>HotSpots</em>
                                    </div>
        <?php endif; ?>
                                <?php if (get_sub_field('partners', $pageID)): ?>
                                    <div class="column">
                                        <span class="counter"><i><?php echo get_sub_field('partners', $pageID); ?></i></span>
                                        <em>Partners</em>
                                    </div>
        <?php endif; ?>
                                <?php if (get_sub_field('features', $pageID)): ?>
                                    <div class="column">
                                        <span class="counter"><?php echo get_sub_field('features', $pageID); ?></span>
                                        <em>Feature</em>
                                    </div>
                            <?php endif; ?>
                            </div>
                            <img src="<?php echo get_sub_field('learn_tab_image', $pageID); ?>" class="tab-image" alt="Image" class="img-responsive">
                        <?php if (get_sub_field('tab_learn_button_link', $pageID)): ?>
                                <a href="<?php echo get_sub_field('tab_learn_button_link', $pageID); ?>" class="btn-primary white"><?php echo get_sub_field('tab_learn_button_text', $pageID); ?></a>
                        <?php endif; ?>
                        </div>
                    <?php
                    $countTab++;
                endwhile;
                ?>
                </div>
<?php } ?>
        </div>
        <div class="col">
            <img src="" alt="tab Image" class="img-responsive changing-image">
        </div>
    </div>
</main>
<?php get_footer(); ?>

<script>

    var locations = <?php echo json_encode($hotspot); ?>;
    console.log(locations);
    var map;
    var markers = [];
    function initMap() {

        map = new google.maps.Map(document.getElementById('map'), {
            zoom: 10,
            center: new google.maps.LatLng(41.6530874,-88.0114749),
            mapTypeId: google.maps.MapTypeId.ROADMAP
        });
        var marker, i;
        var infowindow = new google.maps.InfoWindow();
        for (i = 0; i < locations.length; i++) {
            $(":checkbox[value=" + locations[i].hotspot_strategy + "]").prop("checked", "true");
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i].hotspot_lat, locations[i].hotspot_long),
                map: map,
                title: locations[i].hotspot_strategy
               // icon: locations[i].marker_image
            });
            markers.push(marker);

            var content = locations[i].hotspot_strategy;

            google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
                return function () {
                    infowindow.close();
                    infowindow.setContent(content);
                    infowindow.open(map, marker);
                };
            })(marker, content, infowindow));
        }
    }
    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map
           // icon: location.marker_image
        });
        markers.push(marker);
    }

    // Sets the map on all markers in the array.
    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    // Removes the markers from the map, but keeps them in the array.
    function clearMarkers() {
        setMapOnAll(null);
    }

    // Shows any markers currently in the array.
    function showMarkers() {
        setMapOnAll(map);
    }

    // Deletes all markers in the array by removing references to them.
    function deleteMarkers() {
        clearMarkers();
        markers = [];
    }
    $("#w-live").click(function () {
        $(".we-live").prop('checked', $(this).prop('checked'));
    });

    $("#w-w").click(function () {
        $(".www").prop('checked', $(this).prop('checked'));
    });
    $("#w-play").click(function () {
        $(".w-play").prop('checked', $(this).prop('checked'));
    });
    $("#w-learn").click(function () {
        $(".w-learn").prop('checked', $(this).prop('checked'));
    });

    $("#w-receive").click(function () {
        $(".w-receive").prop('checked', $(this).prop('checked'));
    });
    $("#w-worship").click(function () {
        $(".w-worship").prop('checked', $(this).prop('checked'));
    });
    $(".strategy_check").change(function () {
        filter();

    });

    function filter() {
        deleteMarkers();
        $(".post").hide();
        $.each(locations, function (i, item) {
            radious = $('#textInput').val();
//            console.log(radious)
//            console.log(item.distance + '---' + item.hotspot_place + '++' + item.hotspot_strategy)
            if (radious) {
                if (parseInt(item.distance) <= radious) {
                    $('input[type=checkbox]').each(function () {
                        if (this.checked) {
                            if (this.value == item.hotspot_strategy) {
                                $("[data-strategy='" + item.hotspot_strategy + "']").show();
                                addMarkerInfo(item);
                                return false;
                            }
                        }
                    });
                }
            } else {
                $('input[type=checkbox]').each(function () {
                    if (this.checked) {
                        if (this.value == item.hotspot_strategy) {
                            $("[data-strategy='" + item.hotspot_strategy + "']").show();
                            addMarkerInfo(item);
                            return false;
                        }
                    }
                });

            }
            //                              
        });
        setMapOnAll(map);
    }
    function addMarkerInfo(location) {
        marker = new google.maps.Marker({
            position: new google.maps.LatLng(location.hotspot_lat, location.hotspot_long),
            map: map
            //icon: location.marker_image
        });

        markers.push(marker);
        google.maps.event.addListener(marker, 'click', (function (marker) {
            return function () {
                infowindow.setContent(location.hotspot_strategy);
                infowindow.open(map, marker);
            }
        })(marker));
        var content = location.hotspot_strategy;
        var infowindow = new google.maps.InfoWindow();
        google.maps.event.addListener(marker, 'click', (function (marker, content, infowindow) {
            return function () {
                infowindow.setContent(content);
                infowindow.open(map, marker);
            };
        })(marker, content, infowindow));
    }

    function updateTextInput(val) {
        $('#radious_search').html(val);
        document.getElementById('textInput').value = val;
        filter();
    }
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA7l5RqKcybbGrvSVnI2siEFFuv-VqkuZY&callback=initMap">
</script>