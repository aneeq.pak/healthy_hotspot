<?php
/*
  Template Name: Marketing Materials
 */
get_header();
?>
<main id="main">
	<?php while (have_posts()): the_post(); ?>
	<article class="visual">
		<?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>
		<img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
		<div class="caption">
			<div class="d-table">
				<div class="d-inline">
					<div class="container">
						<?php if(get_field('subheading')):?>
						<strong class="small-title"><?php echo get_field('subheading'); ?></strong>
						<?php endif; ?>
						<h2>
							<?php the_title(); ?>
						</h2>
					</div>
				</div>
			</div>
		</div>
	</article>
	<div class="community container">
		<div class="fullCol animate">
			<?php the_content(); ?>
		</div>
	</div>
	<?php if (is_page('marketing-materials')): ?>
	<div class="discover animate">
		<header class="sec-header">
			<h2>Healthy HotSpot Marketing Materials</h2>
		</header>
		<?php get_template_part('blocks/marketingData');?>
	</div>
	<?php endif; ?>
	
	<?php if (is_page('television')): ?>
	<div class="televisionPanel">
		<div class="container">
			<?php get_template_part('blocks/televisionData');?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if (is_page('radio-commercials')): ?>
	<div class="radioPanel">
		<div class="container">
			<?php get_template_part('blocks/radioData');?>
		</div>
	</div>
	
	<?php endif; ?>
	
	<?php if (is_page('transit-outdoor-ads')): ?>
	<div class="transitPanel">
		<div class="container">
			<?php get_template_part('blocks/transitData');?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if (is_page('online-ads')): ?>
	<div class="onlinePanel">
		<div class="container">
			<?php get_template_part('blocks/onlineadData');?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if (is_page('print')): ?>
	<div class="printPanel">
		<div class="container">
			<?php get_template_part('blocks/printData');?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php if (is_page('social-media-kits')): ?>
	<div class="mediaPanel">
		<div class="container">
			<?php get_template_part('blocks/mediaKits');?>
		</div>
	</div>
	<?php endif; ?>
	
	<?php endwhile; ?>
</main>
<?php get_footer(); ?>
