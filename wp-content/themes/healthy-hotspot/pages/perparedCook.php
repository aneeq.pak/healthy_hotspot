<?php
/*
  Template Name: Prepared Cook Template
 */
get_header();
?>
<main id="main">
    <?php while (have_posts()): the_post(); ?>
    <article class="visual">
        <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()),'full'); ?>
        <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
        <div class="caption">
            <div class="d-table">
                <div class="d-inline">
                    <div class="container">
                        <h2><?php the_title();?></h2>
                    </div>
                </div>
            </div>
        </div>
    </article>
    <div class="community container">
		<div class="cols">
			<div class="col animate">
				<h2><?php echo get_field('first_column_heading');?></h2>
				<?php echo get_field('first_column_description');?>
			</div>
			<div class="col animate">
				<h2><?php echo get_field('second_column_heading');?></h2>
				<?php echo get_field('second_column_description');?>
			</div>
		</div>
	</div>
    <?php if(have_rows('tab_block_section')): ?>
    <div class="discover">
        <ul class="thumbs offers list-none">
            <?php while(have_rows('tab_block_section')): the_row();?>
                <li class="animate">
                <img src="<?php echo get_sub_field('block_image'); ?>" class="img-responsive" alt="Image">
                <div class="caption">
                    <div class="d-table">
                        <div class="d-inline">
                            <img src="<?php echo get_sub_field('block_icon'); ?>" alt="icon">
                            <h2><?php echo get_sub_field('block_heading'); ?></h2>
                            <div class="trans-div">
                                <?php echo get_sub_field('block_text'); ?>
                                <?php if(get_sub_field('block_button_link')):?>
                                    <a href="<?php echo get_sub_field('block_button_link'); ?>" class="btn-primary"><?php echo get_sub_field('block_button_text'); ?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <?php endwhile; ?>
        </ul>
    </div>
    <?php endif; ?>
    <div class="map-area video animate">
       
        <img src="<?php echo get_field('video_section_image');?>" alt="Map" class="img-responsive">
        <div class="caption">
            <a href="#PreparedVideo" class="btn-play btn-popup"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
        </div>
    </div>
    <div class="discover animate">
        <header class="sec-header">
            <strong class="small-title"><?php echo get_field('discover_section_heading');?></strong>
            <h2><?php echo get_field('discover_block_heading');?></h2>
        </header>
        <div class="map-area block">
            <img src="<?php echo get_field('discover_small_image');?>" alt="Star">
            <h2><?php echo get_field('discover_content');?></h2>
            <a href="#" class="btn-primary">Learn more</a>
        </div>
    </div>
    <div class="map-area">
        <img src="<?php echo get_field('map_image');?>" alt="Map" class="img-responsive">
        <div class="caption">
            <div class="d-table">
                <div class="d-inline">
                    <div class="custom-container">
                        <div class="txt">
                            <strong class="small-title"><?php echo get_field('map_section_heading');?></strong>
                            <h2><?php echo get_field('map_block_heading');?></h2>
                        </div>
<?php if(get_field('map_link_url')):?>
                            <a href="<?php echo get_field('map_link_url');?>" class="btn-primary"><?php echo get_field('map_link_text');?></a>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="PreparedVideo" class="popup">
            <div class="d-table">
                <div class="d-inline">
                    <div class="holder container">
                        <div style="display:none"> <?php echo get_field('video_code');?></div>
                        <iframe src="" frameborder="0"></iframe>
                        <a href="#" class="popup-closer"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile;?>
    
</main>
<?php get_footer(); ?>