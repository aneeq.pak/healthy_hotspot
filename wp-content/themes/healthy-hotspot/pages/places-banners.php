<?php
/*
  Template Name: Places With Banner Template
 */
get_header();
?>
<main id="main">

    <section class="top-tiles">
        <div class="col">
            <ul class="tiles list-none">
                <li>
                    <div class="holder" style="background:url('<?php echo get_field('small_half_image'); ?>') no-repeat; background-size:cover;">
                        <!-- <img src="<?php //echo get_field('small_half_image');         ?>" alt="Image" class="img-responsive"> -->
                        <a href="<?php echo get_field('small_half_1_button_link'); ?>" class="caption">
                            <div class="d-table">
                                <div class="d-inline">
                                    <?php
                                    $sHeading = get_field('small_half_heading');
                                    if ($sHeading):
                                        ?>
                                        <strong class="small-title">connect to</strong>
                                        <h2 class="<?php echo get_field('small_half_1_class'); ?>"><?php echo $sHeading; ?></h2>
                                    <?php endif; ?>
                                    <?php echo get_field('small_half_text'); ?>
                                    <?php if (get_field('small_half_1_button_link')) { ?>
                                        <span class="btn-primary green"><?php echo get_field('small_half_1_button_text'); ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="holder" style="background:url('<?php echo get_field('small_half_image_2'); ?>') no-repeat; background-size:cover;">
                        <!-- <img src="<?php //echo get_field('small_half_image_2');         ?>" alt="Image" class="img-responsive"> -->
                        <a href="<?php echo get_field('small_half_2_button_link'); ?>" class="caption">
                            <div class="d-table">
                                <div class="d-inline">
                                    <?php
                                    $sHeading2 = get_field('small_half_heading_2');
                                    if ($sHeading2):
                                        ?>
                                        <strong class="small-title">connect to</strong>
                                        <h2 class="<?php echo get_field('small_half_2_class'); ?>"><?php echo $sHeading2; ?></h2>
                                    <?php endif; ?>
                                    <?php echo get_field('small_half_text_2'); ?>
                                    <?php if (get_field('small_half_2_button_link')) { ?>
                                        <span class="btn-primary blue"><?php echo get_field('small_half_2_button_text'); ?></span>
                                    <?php } ?>
                                </div>
                            </div>
                        </a>
                    </div>
                </li>
                <li>
                    <div class="holder" style="background:url('<?php echo get_field('small_large_image'); ?>') no-repeat; background-size:cover;">
                        <!-- <img src="<?php //echo get_field('small_large_image');         ?>" alt="Image" class="img-responsive"> -->


                        <?php
                        if (get_field('small_large_button_link') == "#"):
                            ?>
                            <a href="#common-popup" class="caption btn-vid btn-popup">
                                <?php
                            else:
                                ?>
                                <a href="<?php echo get_field('small_large_button_link'); ?>" class="caption">
                                <?php
                                endif;
                                ?>    
                                <div class="d-table">
                                    <div class="d-inline">
                                        <?php
                                        $sLHeading = get_field('small_large_heading');
                                        if ($sLHeading):
                                            ?>
                                            <strong class="small-title">connect to</strong>
                                            <h2 class="<?php echo get_field('small_large_class'); ?>"><?php echo $sLHeading; ?></h2>
                                        <?php endif; ?>
                                        <?php echo get_field('small_large_text'); ?>
                                        <?php if (get_field('small_large_button_link')) { ?>
                                            <span class="btn-primary gold"><?php echo get_field('small_large_button_text'); ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </a>
                    </div>
                </li>
            </ul>
        </div>
        <div class="col" style="background:url('<?php echo get_field('large_image'); ?>') no-repeat; background-size:cover;">
            <!-- <img src="<?php //echo get_field('large_image');         ?>" alt="Image" class="img-responsive"> -->
            <a href="<?php echo get_field('large_button_link'); ?>" class="caption add">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="txt">
                            <?php
                            $LHeading = get_field('large_heading');
                            if ($LHeading):
                                ?>
                                <strong class="small-title">connect to</strong>
                                <h3><?php echo $LHeading; ?></h3>
                            <?php endif; ?>
                            <?php echo get_field('large_text'); ?>
                            <?php if (get_field('large_button_link')) { ?>
                                <span class="btn-primary"><?php echo get_field('large_button_text'); ?></span>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </a>
        </div>
    </section>


    <?php while (have_posts()): the_post(); ?>
        <!--<article class="visual">
            <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>    
            <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="container">
                            <?php if (get_field('subheading')): ?>
                                <strong class="small-title"><?php echo get_field('subheading'); ?></strong>
                            <?php endif; ?>
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </article>-->
        <div class="community container">
            <div class="cols">
                <div class="col animate">
                    <h2><?php echo get_field('first_column_heading'); ?></h2>
                    <?php echo get_field('first_column_description'); ?>
                </div>
                <div class="col animate">
                    <h2><?php echo get_field('second_column_heading'); ?></h2>
                    <?php echo get_field('second_column_description'); ?>
                </div>
            </div>
        </div>
        <div class="discover animate">
            <header class="sec-header">
                <strong class="small-title"><?php echo get_field('discover_main_heading'); ?></strong>
                <h2><?php echo get_field('discover_subheading'); ?></h2>
            </header>
            <?php get_template_part('blocks/discoverydata'); ?>
        </div>
        <div class="map-area">
            <img src="<?php echo get_field('map_image'); ?>" alt="Map" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="custom-container">
                            <div class="txt">
                                <strong class="small-title"><?php echo get_field('map_section_heading'); ?></strong>
                                <h2><?php echo get_field('map_block_heading'); ?></h2>
                            </div>
                            <?php if (get_field('map_link')): ?>
                                <a href="<?php echo get_field('map_link'); ?>" class="btn-primary"><?php echo get_field('map_link_text'); ?></a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>