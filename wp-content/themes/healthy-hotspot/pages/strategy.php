<?php
/*
  Template Name: Strategy Template
 */
get_header();
?>
<main id="main">
    <?php while (have_posts()): the_post(); ?>
        <article class="visual">
            <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>    
            <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="container">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <div class="community container">
                <div class="cols">
                    <div class="col">
                        <h2><?php echo get_field('first_column_heading');?></h2>
                        <?php echo get_field('first_column_description');?>
                    </div>
                    <div class="col">
                        <h2><?php echo get_field('second_column_heading');?></h2>
                        <?php echo get_field('second_column_description');?>
                    </div>
                </div>
            </div>
            <div class="discover">
                <header class="sec-header">
                    <strong class="small-title"><?php echo get_field('discover_main_heading');?></strong>
                    <h2><?php echo get_field('discover_subheading');?></h2>
                </header>
             <?php get_template_part( 'blocks/discovery' ); ?>
            </div>
            <div class="map-area">
                <img src="<?php echo get_field('map_image');?>" alt="Map" class="img-responsive">
                <div class="caption">
                    <div class="d-table">
                        <div class="d-inline">
                            <div class="custom-container">
                                <div class="txt">
                                    <strong class="small-title"><?php echo get_field('map_section_heading');?></strong>
                                    <h2><?php echo get_field('map_block_heading');?></h2>
                                </div>
                                <?php if(get_field('map_link')):?>
                                    <a href="<?php echo get_field('map_link');?>" class="btn-primary"><?php echo get_field('map_link_text');?></a>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>