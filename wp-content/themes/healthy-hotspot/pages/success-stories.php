<?php
/*
  Template Name: Success Stories Template
 */
get_header();
?>
<main id="main">
    <?php while (have_posts()): the_post(); ?>
        <article class="visual">
            <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()),'full') ?>    
            <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="container">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <div class="discover">
            <?php $arg = array(
                'post_type' => 'stories'
            );
            $query = new Wp_Query($arg); 
            ?>
            <?php if($query->have_posts()):?>
                <ul class="discover-tiles full-opacity list-none">
                <?php while($query->have_posts()): $query->the_post();?>
                    <li class="animate">
                        <?php $simg = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()),'full') ?>    
                    <div class="align-left">
                        <img src="<?php echo $simg; ?>" alt="Image" class="img-responsive">
                    </div>
                    <div class="align-right">
                        <div class="d-table">
                            <div class="d-inline">
                                <h3><?php the_title(); ?></h3>
                                <?php the_excerpt();?>
                                <a href="<?php the_permalink();?>" class="btn-primary">read more</a>
                            </div>
                        </div>
                    </div>
                </li>
                <?php endwhile; ?>
            </ul>
            <?php endif; wp_reset_postdata();?>
        </div>
        <div class="map-area add animate">
            <img src="<?php the_field('story_box_image');?>" alt="Map" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="custom-container">
                            <div class="txt">
                                <strong class="small-title"><?php the_field('story_box_heading');?></strong>
                                <h2><?php the_field('story_block_heading');?></h2>
                            </div>
                            <a href="<?php the_field('story_button_link');?>" class="btn-primary"><?php the_field('story_button_text');?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>