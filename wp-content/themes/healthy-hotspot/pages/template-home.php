<?php
/*
  Template Name: Home Template
 */
get_header();
?>
<main id="main">
    <?php while (have_posts()): the_post(); ?>
        <section class="top-tiles">
            <div class="col">
                <ul class="tiles list-none">
                    <li>
                        <div class="holder" style="background:url('<?php echo get_field('small_half_image'); ?>') no-repeat; background-size:cover;">
                            <!-- <img src="<?php //echo get_field('small_half_image');        ?>" alt="Image" class="img-responsive"> -->
                            <a href="<?php echo get_field('small_half_1_button_link'); ?>" class="caption">
                                <div class="d-table">
                                    <div class="d-inline">
                                        <?php
                                        $sHeading = get_field('small_half_heading');
                                        if ($sHeading):
                                            ?>
                                            <strong class="small-title">connect to</strong>
                                            <h2 class="<?php echo get_field('small_half_1_class'); ?>"><?php echo $sHeading; ?></h2>
                                        <?php endif; ?>
                                        <?php echo get_field('small_half_text'); ?>
                                        <?php if (get_field('small_half_1_button_link')) { ?>
                                            <span class="btn-primary green"><?php echo get_field('small_half_1_button_text'); ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="holder" style="background:url('<?php echo get_field('small_half_image_2'); ?>') no-repeat; background-size:cover;">
                            <!-- <img src="<?php //echo get_field('small_half_image_2');        ?>" alt="Image" class="img-responsive"> -->
                            <a href="<?php echo get_field('small_half_2_button_link'); ?>" class="caption">
                                <div class="d-table">
                                    <div class="d-inline">
                                        <?php
                                        $sHeading2 = get_field('small_half_heading_2');
                                        if ($sHeading2):
                                            ?>
                                            <strong class="small-title">connect to</strong>
                                            <h2 class="<?php echo get_field('small_half_2_class'); ?>"><?php echo $sHeading2; ?></h2>
                                        <?php endif; ?>
                                        <?php echo get_field('small_half_text_2'); ?>
                                        <?php if (get_field('small_half_2_button_link')) { ?>
                                            <span class="btn-primary blue"><?php echo get_field('small_half_2_button_text'); ?></span>
                                        <?php } ?>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </li>
                    <li>
                        <div class="holder" style="background:url('<?php echo get_field('small_large_image'); ?>') no-repeat; background-size:cover;">
                            <!-- <img src="<?php //echo get_field('small_large_image');        ?>" alt="Image" class="img-responsive"> -->


                            <?php
                            if (get_field('small_large_button_link') == "#"):
                                ?>
                                <a href="#common-popup" class="caption btn-vid btn-popup">
                                    <?php
                                else:
                                    ?>
                                    <a href="<?php echo get_field('small_large_button_link'); ?>" class="caption">
                                    <?php
                                    endif;
                                    ?>    
                                    <div class="d-table">
                                        <div class="d-inline">
                                            <?php
                                            $sLHeading = get_field('small_large_heading');
                                            if ($sLHeading):
                                                ?>
                                                <strong class="small-title">connect to</strong>
                                                <h2 class="<?php echo get_field('small_large_class'); ?>"><?php echo $sLHeading; ?></h2>
                                            <?php endif; ?>
                                            <?php echo get_field('small_large_text'); ?>
                                            <?php if (get_field('small_large_button_link')) { ?>
                                                <span class="btn-primary gold"><?php echo get_field('small_large_button_text'); ?></span>
                                            <?php } ?>
                                        </div>
                                    </div>
                                </a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col" style="background:url('<?php echo get_field('large_image'); ?>') no-repeat; background-size:cover;">
                <!-- <img src="<?php //echo get_field('large_image');        ?>" alt="Image" class="img-responsive"> -->
                <a href="<?php echo get_field('large_button_link'); ?>" class="caption add">
                    <div class="d-table">
                        <div class="d-inline">
                            <div class="txt">
                                <?php
                                $LHeading = get_field('large_heading');
                                if ($LHeading):
                                    ?>
                                    <strong class="small-title">connect to</strong>
                                    <h3><?php echo $LHeading; ?></h3>
                                <?php endif; ?>
                                <?php echo get_field('large_text'); ?>
                                <?php if (get_field('large_button_link')) { ?>
                                    <span class="btn-primary"><?php echo get_field('large_button_text'); ?></span>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </a>
            </div>
        </section>
        <div class="about-holder container animate">
            <?php
            $hBImg = get_field('healthier_block_top_image');
            if ($hBImg):
                ?>
                <img src="<?php echo $hBImg; ?>" alt="Image">
            <?php endif; ?>
            <?php
            $hSh = get_field('healthier_section_heading');
            if ($hSh):
                ?>    
                <strong class="small-title"><?php echo get_field('healthier_section_heading'); ?></strong>
            <?php endif; ?>
            <h2><?php echo get_field('healthier_block_heading_'); ?></h2>
            <div class="txt">
                <?php echo get_field('healthier_block_text'); ?>
            </div>
            <?php
            $healthLink = get_field('healthier_block_button_link');
            if ($healthLink):
                ?>
                <a href="<?php echo $healthLink; ?>" class="btn-primary"> <?php echo get_field('healthier_block_button_text'); ?></a>
            <?php endif; ?>
        </div>
        <div class="discover animate">
            <!-- <header class="sec-header">
                <strong class="small-title"><?php //echo get_field('discover_section_heading');         ?></strong>
                <h2><?php //echo get_field('discover_block_heading');      ?></h2>
            </header> -->
            <?php if (have_rows('discover_section_content')): ?>
                <ul class="discover-tiles discoverRow list-none">
                    <?php while (have_rows('discover_section_content')) : the_row(); ?>
                        <li style="background: url('<?php echo get_sub_field('discover_image'); ?>');">
                            <!--<img src="<?php // echo get_sub_field('discover_image');        ?>" alt="Image" class="img-responsive">-->
                            <div class="caption <?php echo get_sub_field('discover_class'); ?>">
                                <div class="d-table">
                                    <div class="d-inline">
                                        <div class="icon"><img src="<?php echo get_sub_field('discover_icone'); ?>" alt="Image"></div>
                                        <strong><?php echo get_sub_field('discover_small_heading'); ?></strong>
                                        <h3><?php echo get_sub_field('discover_large_heading'); ?></h3>
                                        <div class="trans-div">
                                            <?php echo get_sub_field('discover_text'); ?>
                                            <?php if (get_sub_field('discover_button_link')): ?>
                                                <a href="<?php echo get_sub_field('discover_button_link'); ?>" class="btn-primary white"><?php echo get_sub_field('dicover_button_text'); ?></a>
                                            <?php endif; ?>
                                        </div>    
                                    </div>
                                </div>
                            </div>
                        </li>
                    <?php endwhile; ?>
                </ul>
            <?php endif; ?>
        </div>
        <div class="home-tabbing animate">
            <div class="col">
                <strong class="small-title"><?php echo get_field('learning_section_heading'); ?></strong>
                <h1><?php echo get_field('learning_block_heading'); ?></h1>
                <?php if (have_rows('learning_block_tabbing')) { ?>
                    <ul class="home-tabs list-none">
                        <?php
                        $count = 1;
                        while (have_rows('learning_block_tabbing')): the_row();
                            ?>
                            <li <?php
                            if ($count == 1) {
                                echo 'class="active"';
                            }
                            ?>><a href="#<?php echo get_sub_field('learn_tab_id'); ?>"><?php echo get_sub_field('learn_tab_heading'); ?></a></li>
                                <?php
                                $count++;
                            endwhile;
                            ?>
                    </ul>
                <?php } ?>
                <?php if (have_rows('learning_block_tabbing')) { ?>
                    <div class="home-tab-content">
                        <?php
                        $countTab = 1;
                        while (have_rows('learning_block_tabbing')): the_row();
                            ?>
                            <div id="<?php echo get_sub_field('learn_tab_id'); ?>" class="tab <?php if ($countTab == 1) echo 'active'; ?>">
                                <?php echo get_sub_field('learn_tab_text'); ?>
                                <div class="three-cols">
                                    <?php if (get_sub_field('hotspot_figure')): ?>
                                        <div class="column">
                                            <span class="counter"><i><?php echo get_sub_field('hotspot_figure'); ?></i></span>
                                            <em>HotSpots</em>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('partners')): ?>
                                        <div class="column">
                                            <span class="counter"><i><?php echo get_sub_field('partners'); ?></i></span>
                                            <em>Partners</em>
                                        </div>
                                    <?php endif; ?>
                                    <?php if (get_sub_field('features')): ?>
                                        <div class="column">
                                            <span class="counter"><?php echo get_sub_field('features'); ?></span>
                                            <em>Feature</em>
                                        </div>
                                    <?php endif; ?>
                                </div>
                                <img src="<?php echo get_sub_field('learn_tab_image'); ?>" class="tab-image" alt="Image" class="img-responsive">
                                <?php if (get_sub_field('tab_learn_button_link')): ?>
                                    <a href="<?php echo get_sub_field('tab_learn_button_link'); ?>" class="btn-primary white"><?php echo get_sub_field('tab_learn_button_text'); ?></a>
                                <?php endif; ?>
                            </div>
                            <?php
                            $countTab++;
                        endwhile;
                        ?>
                    </div>
                <?php } ?>
            </div>
            <div class="col">
                <img src="" alt="tab Image" class="img-responsive changing-image">
            </div>
        </div>
        <div class="find-area animate">
            <div class="col">
                <div class="col-holder">
                    <img src="<?php echo get_field('map_image'); ?>" class="img-responsive" alt="map image">
                </div>
            </div>
            <div class="col">
                <div class="d-table">
                    <div class="d-inline">
                        <strong class="small-title"><?php echo get_field('map_section_heading'); ?></strong>
                        <h2><?php echo get_field('map_block_heading'); ?></h2>
                        <p><?php echo get_field('map_block_text'); ?></p>
                        <?php
                        $mLink = get_field('map_button_link');
                        if ($mLink):
                            ?>
                            <a href="<?php echo $mLink; ?>" class="btn-primary"><?php echo get_field('map_button_text'); ?></a>
                        <?php endif; ?>    
                    </div>
                </div>
            </div>
        </div>
        <div class="twocolumns animate">
            <div class="column">
                <strong class="small-title"><?php echo get_field('success_section_heading'); ?></strong>
                <h2><?php echo get_field('success_block_heading'); ?></h2>
                <img src="<?php echo get_field('success_block_image'); ?>" alt="Image">
                <a href="http://www.cookcountypublichealth.org/healthy-hotspot/success-stories" target="_blank" class="btn-primary">Learn more</a>
            </div>
            <div class="column">
                <strong class="small-title"><?php echo get_field('featured_section_heading'); ?></strong>
                <h2><?php echo get_field('featured_block_heading'); ?></h2>
                <img src="<?php echo get_field('featured_block_image'); ?>" alt="Image">
                <a href="http://www.cookcountypublichealth.org/healthy-hotspot/partners" target="_blank" class="btn-primary">Learn more</a>
            </div>
        </div>
    <?php endwhile; ?>
</main>
<?php get_footer(); ?>