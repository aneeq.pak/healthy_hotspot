<?php get_header(); ?>
<main id="main">
    <?php while (have_posts()) : the_post(); ?>
        <article class="visual">
            <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()), 'full') ?>    
            <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">

            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="container">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </article>
        <div class="community container">
            <div class="cols">
                <div class="col">
                    <h2><?php echo get_field('first_block_heading', get_the_ID()); ?></h2>
                    <?php echo get_field('first_block_content', get_the_ID()); ?>

                </div>
                <div class="col">
                    <h2>"<?php echo get_field('second_block_heading', get_the_ID()); ?>"</h2>
                    <?php echo get_field('second_block_content', get_the_ID()); ?>
                </div>
            </div>
        </div>
        <div class="map-area video">
            <img src="<?php echo get_field('video_block_image', get_the_ID()); ?>" alt="Map" class="img-responsive">
            <div class="caption">
                <a href="#mapVideo" class="btn-play btn-popup"><i class="fa fa-caret-right" aria-hidden="true"></i></a>
            </div>
        </div>
        <?php if (get_field('success_stories_discover_heading', 'options')): ?>
            <div class="discover">
                <header class="sec-header">
                    <strong class="small-title"><?php echo get_field('success_stories_discover_heading', 'options'); ?></strong>
                    <h2><?php echo get_field('success_stories_discover_content', 'options'); ?></h2>
                </header>
                <?php get_template_part('blocks/discoverydata'); ?>
            </div>
        <?php endif; ?>
        <?php $partnersSlider = get_field('supporting_partners');
            if ($partnersSlider):
                ?>
        <div class="discover add">
            <header class="sec-header add container">
                <span class="align-right"><?php echo get_field('partners_block_content'); ?></span>
                <h2><?php echo get_field('partners_block_heading'); ?></h2>
            </header>
           
                <div class="flexslider carousel">
                    <ul class="slides partners-list list-none">
                            <?php foreach ($partnersSlider as $partners): ?>
                            <li>
                                    <?php if ($partners['partners_link']): ?>
                                    <a href="<?php echo $partners['partners_link']; ?>" >
                                    <?php endif; ?>
                                    <img src="<?php echo $partners['parnters_image']; ?>" alt="Logo">
                                <?php if ($partners['partners_link']): ?>
                                    </a>
                            <?php endif; ?>
                            </li>       
                <?php endforeach; ?>
                    </ul>
                </div>
          </div>
        <?php endif; ?>
        <div class="map-area">
            <img src="<?php echo get_field('success_map_image', 'options'); ?>" alt="Map" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="custom-container">
                            <div class="txt">
                                <strong class="small-title"><?php echo get_field('success_map_heading', 'options'); ?></strong>
                                <h2><?php echo get_field('success_map_content', 'options'); ?></h2>
                            </div>
                            <a href="<?php echo get_field('success_map_link', 'options'); ?>" class="btn-primary"><?php echo get_field('success_map_link_text', 'options'); ?></a>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="mapVideo" class="popup">
            <div class="d-table">
                <div class="d-inline">
                    <div class="holder container">
                        <div style="display:none"><?php echo get_field('video_block_video', get_the_ID()); ?>?rel=0</div>
                        <iframe src="" frameborder="0"></iframe>
                        <a href="#" class="popup-closer"><i class="fa fa-times-circle" aria-hidden="true"></i></a>
                    </div>
                </div>
            </div>
        </div>
    <?php endwhile; ?>
</main>

<?php get_footer(); ?>