<?php get_header(); ?>
<?php while ( have_posts() ) : the_post(); ?>
 <article class="visual">
            <?php $img = wp_get_attachment_image_url(get_post_thumbnail_id(get_the_ID()),'full') ?>    
            <img src="<?php echo $img; ?>" alt="Promo image" class="img-responsive">
            <div class="caption">
                <div class="d-table">
                    <div class="d-inline">
                        <div class="container">
                            <h2><?php the_title(); ?></h2>
                        </div>
                    </div>
                </div>
            </div>
        </article>
 <div class="community container">
		
			<?php get_template_part( 'blocks/content', get_post_type() ); ?>
			<?php comments_template(); ?>
			<?php get_template_part( 'blocks/pager-single', get_post_type() ); ?>
 </div>
		<?php endwhile; ?>

<?php get_footer(); ?>